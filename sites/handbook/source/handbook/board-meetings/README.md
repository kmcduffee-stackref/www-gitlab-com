# Board of Directors and Corporate Governance has been migrated

This handbook content has been migrated to the new handbook site and as such this directory
has been locked from further changes.

Viewable content: [https://handbook.gitlab.com/handbook/board-meetings](https://handbook.gitlab.com/handbook/board-meetings)
Repo Location: [https://gitlab.com/gitlab-com/content-sites/handbook/-/tree/main/content/handbook/board-meetings](https://gitlab.com/gitlab-com/content-sites/handbook/-/tree/main/content/handbook/board-meetings)

If you need help or assitance with this please reach out to @jamiemaynard (Developer/Handbooks) or
@marshall007 (DRI Content Sites).  Alternatively ask your questions on slack in [#handbook](https://gitlab.slack.com/archives/C81PT2ALD)

